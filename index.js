const jwt = require('jsonwebtoken');
const fs = require('fs');
const httpReq = require('request');

function AuthHelper(publicKeyUrl) {
    this.privateKeyLocation = "";
    this.publicKeyUrl = publicKeyUrl;
    this.options = {
        expiresIn: '1d',
        algorithm: 'RS256'
    }

    let self = this;

    this.signJwt = function(data) {
        const privateKey = fs.readFileSync(self.privateKeyLocation, 'utf8');
        return jwt.sign(data, privateKey, self.options);
    }

    this.validateJwt = function(request, response, next) {
        console.log("PUBKEY")
        console.log(self.publicKeyUrl)
        const token = request.get('Authorization').substring(4) // Ignores "JWT "
    
        httpReq.get(self.publicKeyUrl, (httperr, res, publicKey) => {
            if (httperr) throw httperr;
    
            jwt.verify(token, publicKey, self.options, (err, decoded) => {
                if (err) {
                    response.locals.decoded = {}
                    return response.status('403').json({
                        success: false,
                        error: err
                    });
                }
                response.locals.decoded = decoded;
                next();
            })
        })
    }
}

module.exports = AuthHelper;